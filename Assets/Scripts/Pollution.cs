﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pollution : MonoBehaviour
{
    List<Tile> PollutedTiles;

    private void Start()
    {
        //Init();
    }

    public void Init()
    {
        GameManager GM = GameManager.Get();

        Tile[,] Tiles = GM.Tiles;

        PollutedTiles = new List<Tile>();

        for (int i = 0; i < Tiles.GetLength(0); i++)
        {
            for (int j = 0; j < Tiles.GetLength(1); j++)
            {
                if (Tiles[i, j].GetHealth() < 0)
                {
                    PollutedTiles.Add(Tiles[i, j]);
                }
            }
        }
    }
    public Tile NextStep()
    {
        int Rand = Random.Range(0, PollutedTiles.Count);
        //Rand = 0;
        int Count = 0;

        while (Count < PollutedTiles.Count && !PollutedTiles[Rand].CanBePolluted())
        {
            Rand = (Rand + 1) % PollutedTiles.Count;
            ++Count;
        }

        if (Count < PollutedTiles.Count)
        {
            GameManager GM = GameManager.Get();
            Tile CenterTile = PollutedTiles[Rand];
            CenterTile.Pollute();
            Tile LeftTile = GM.GetTile(new Vector2Int(CenterTile.Position.x - 1, CenterTile.Position.y));
            if (LeftTile)
            {
                LeftTile.Pollute();
                if (LeftTile.GetHealth() < 0)
                {
                    PollutedTiles.Add(LeftTile);
                }
            }
            Tile RightTile = GM.GetTile(new Vector2Int(CenterTile.Position.x + 1, CenterTile.Position.y));
            if (RightTile)
            {
                RightTile.Pollute();
                if (RightTile.GetHealth() < 0)
                {
                    PollutedTiles.Add(RightTile);
                }
            }
            Tile DownTile = GM.GetTile(new Vector2Int(CenterTile.Position.x, CenterTile.Position.y - 1));
            if (DownTile)
            {
                DownTile.Pollute();
                if (DownTile.GetHealth() < 0)
                {
                    PollutedTiles.Add(DownTile);
                }
            }
            Tile UpTile = GM.GetTile(new Vector2Int(CenterTile.Position.x, CenterTile.Position.y + 1));
            if (UpTile)
            {
                UpTile.Pollute();
                if (UpTile.GetHealth() < 0)
                {
                    PollutedTiles.Add(UpTile);
                }
            }

            return CenterTile;
        }
        else return null;
    }
}
