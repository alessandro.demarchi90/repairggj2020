﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject infoPanel;
    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void HowToPlay()
    {
        infoPanel.SetActive(true);
    }

    public void RetToMenu() 
    {
        infoPanel.SetActive(false);
    }
}
