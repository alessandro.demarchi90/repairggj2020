﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public partial class GameManager : MonoBehaviour
{
    [SerializeField]
    public Vector2Int FieldSize;

    [HideInInspector] public Tile[,] Tiles { get; private set; }

    public int SeedNumber;

    private static GameManager Instance = null;

    public GameObject BoardCell;

    public Vector2Int PollutionStartPositionMin;
    public Vector2Int PollutionStartPositionMax;

    public Grid Mygrid;

    [HideInInspector]
    public float
        TileScale,
        TileSize;

    public float DelayTime;
    public int TimeCritical;

    private int TimeCounter;

    private Pollution Pol;
    public ViewPollution ViewPol;
    private ViewUI vUI;
    public float losePercentage;

    public GameObject FactorySprite;

    private void Awake()
    {
        Time.timeScale = 1f;
        Tiles = new Tile[FieldSize.y, FieldSize.x];
        Pol = FindObjectOfType<Pollution>();
        ViewPol = FindObjectOfType<ViewPollution>();
        vUI = FindObjectOfType<ViewUI>();
    }

    void SetCorrectTileScaleAndTileSize()
    {
        //I calculate the correct resize of the tiles in order to make the board fill in the screen (maximum width: 90%
        //of the screen width; maximum height: 60% of the screen height). 

        float SceneMultiplier;
        int BaseOrHeightNumberOptimalForResize;
        //if (9f/10f * (float)Screen.width / Screen.height <= 3f/5f * (float)BaseCellNumber / HeightCellNumber)
        //{
        //    SceneMultiplier = 9f/10f *  (float)(Camera.main.orthographicSize * 2f) * Screen.width / Screen.height;
        //    BaseOrHeightNumberOptimalForResize = BaseCellNumber;
        //}
        //else
        //{
        SceneMultiplier = (float)(Camera.main.orthographicSize * 2f);
        BaseOrHeightNumberOptimalForResize = FieldSize.y;
        //}

        float InitialBoardCellScale = BoardCell.transform.localScale.x;

        //I create the board in order to be the 90% of the screen
        TileScale = InitialBoardCellScale * SceneMultiplier / BaseOrHeightNumberOptimalForResize;
        TileSize = BoardCell.GetComponent<Renderer>().bounds.size.x * SceneMultiplier / BaseOrHeightNumberOptimalForResize;

    } //SetCorrectTileScaleAndTileSize

    private void Start()
    {
        //SetCorrectTileScaleAndTileSize();
        InitializeBoard();
        StartCoroutine(NextStep());
        Camera.main.GetComponent<touchInputMovement>().Initialize();
    }




    void InitializeBoard()
    {
        TileScale = BoardCell.transform.localScale.x;
        TileSize = BoardCell.GetComponent<Renderer>().bounds.size.x;
        //The following script is thought in order to generate a centered board 
        /*float firstCellXPosition = -TileSize * (BaseCellNumber - 1) / 2f;
        float firstCellYPosition = -TileSize * (HeightCellNumber - 1) / 2f;*/
        float firstCellXPosition = TileSize / 2;
        float firstCellYPosition = TileSize / 2;

        Camera.main.transform.position = new Vector3(TileSize * (FieldSize.x - 1) / 2f, TileSize * (FieldSize.y - 1) / 2f, -10);

        bool isFirstFactoryCell = true;

        for (int i = 0; i < FieldSize.y; i++)
        {
            for (int j = 0; j < FieldSize.x; j++)
            {
                GameObject Cell = Instantiate(BoardCell,
                    new Vector2(firstCellXPosition + j * TileSize,
                                firstCellYPosition + i * TileSize), Quaternion.identity);

                /* GameObject Cell = Instantiate(BoardCell,

                     Mygrid.CellToWorld(new Vector3Int(i,j,1)

                     ), Quaternion.identity);*/



                Cell.transform.localScale = new Vector3(TileScale, TileScale);
                Cell.name = i + "," + j;
                Cell.transform.SetParent(transform);
                Tile CurrentTileScript = Cell.GetComponent<Tile>();
                CurrentTileScript.Init(Tiles, new Vector2Int(i, j));

                if (j >= PollutionStartPositionMin[0] && j <= PollutionStartPositionMax[0] && i >= PollutionStartPositionMin[1] && i <= PollutionStartPositionMax[1])
                {
                   
                    CurrentTileScript.Pollute();
                    Cell.GetComponent<Collider2D>().enabled = false;

 
                  if (isFirstFactoryCell)
                    {
                        GameObject Factory = Instantiate(FactorySprite, Cell.transform);
                        Factory.transform.localScale = new Vector3(1f, 1f, 0);
                        Factory.transform.position = new Vector3(Factory.transform.position.x + TileSize/2,
                            Factory.transform.position.y + TileSize/2,
                            Factory.transform.position.z);
                        isFirstFactoryCell = false;

                    }

              
                }

                Tiles[i, j] = CurrentTileScript;


            }
        }
        Pol.Init();
        Mygrid.cellSize = new Vector3(Mygrid.cellSize.x * TileSize, Mygrid.cellSize.y * TileSize, Mygrid.cellSize.z * TileSize);

    } //InitializeBoard

    public static GameManager Get()
    {
        if (!Instance)
        {
            Instance = FindObjectOfType<GameManager>();
        }
        return Instance;
    }

    public Tile GetTile(Vector2Int InPosition)
    {
        if (InPosition.x >= 0 && InPosition.x < Tiles.GetLength(0) && InPosition.y >= 0 && InPosition.y < Tiles.GetLength(1))
        {
            return Tiles[InPosition.x, InPosition.y];
        }
        return null;
    }

    public bool CanPlant(Tile Tile)
    {
        return Tile && Tile.Plant == null && Tile.GetHealth() >= 0;
    }

    public void PlantTree(Plant Type, Tile Tile)
    {
        if (CanPlant(Tile))
        {
            Tile.PlantTree(Type);
            if (Type.PlantName == MyTags.OXIGEN_PLANT_NAME) Tile.Plant.InfluenceArea((Tile _Tile) => { _Tile.Oxygenate(); });
            if (Type.PlantName == MyTags.FRUIT_PLANT_NAME) Tile.Oxygenate();
            viewUpdate();
            
            //codice di gestione lo stop di animazione
        }
    }

   /*public void CheckIfStillSuffering(Tile tile)
    {
        Tile LeftTile = GetTile(new Vector2Int(tile.Position.x - 1, tile.Position.y));
        if (LeftTile.Plant.isSuffering && LeftTile.GetHealth() > 0)
        {
            LeftTile.Plant.StopSuffering();
        }
        Tile RightTile = GetTile(new Vector2Int(tile.Position.x + 1, tile.Position.y));
        if (RightTile.Plant.isSuffering && RightTile.GetHealth() > 0)
        {
            LeftTile.Plant.StopSuffering();
        }
        Tile DownTile = GetTile(new Vector2Int(tile.Position.x, tile.Position.y - 1));
        if (DownTile.Plant.isSuffering && DownTile.GetHealth() > 0)
        {
            DownTile.Plant.StopSuffering();
        }
        Tile UpTile = GetTile(new Vector2Int(tile.Position.x, tile.Position.y + 1));
        if (UpTile.Plant.isSuffering && UpTile.GetHealth() > 0)
        {
            UpTile.Plant.StopSuffering();
        }
    }*/

    public void UpdateKillAndSuffering(Tile tile)
    {
        Tile LeftTile = GetTile(new Vector2Int(tile.Position.x - 1, tile.Position.y));
        if (LeftTile && LeftTile.Plant)
        {
            if(LeftTile.GetHealth() < 0)
            {
                KillPlant(LeftTile.Plant);
            }
            if(LeftTile.GetHealth() == 0 && LeftTile.Oxygen != 3)
            {
                LeftTile.Plant.StartSuffering();
            }
        }
        Tile RightTile = GetTile(new Vector2Int(tile.Position.x + 1, tile.Position.y));
        if (RightTile && RightTile.Plant)
        {
            if (RightTile.GetHealth() < 0)
            {
                KillPlant(RightTile.Plant);
            }
            if (RightTile.GetHealth() == 0 && RightTile.Oxygen != 3)
            {
                RightTile.Plant.StartSuffering();
            }
        }
        Tile DownTile = GetTile(new Vector2Int(tile.Position.x, tile.Position.y - 1));
        if (DownTile && DownTile.Plant)
        {
            if (DownTile.GetHealth() < 0)
            {
                KillPlant(DownTile.Plant);
            }
            if (DownTile.GetHealth() == 0 && DownTile.Oxygen != 3)
            {
                DownTile.Plant.StartSuffering();
            }
        }
        Tile UpTile = GetTile(new Vector2Int(tile.Position.x, tile.Position.y + 1));
        if (UpTile && UpTile.Plant)
        {
            if (UpTile.GetHealth() < 0)
            {
                KillPlant(UpTile.Plant);
            }
            if (UpTile.GetHealth() == 0 && UpTile.Oxygen != 3)
            {
                UpTile.Plant.StartSuffering();
            }
        }
    }

    public void KillPlant(Plant Plant)
    {
        if (Plant && !Plant.PendingKill)
        {
            if (Plant.PlantName == MyTags.OXIGEN_PLANT_NAME) Plant.InfluenceArea((Tile _Tile) => { _Tile.Deoxygenate(); });
            else Plant.Tile.Deoxygenate();

            Plant.Die();
        }
    }

    IEnumerator NextStep()
    {
        while (true)
        {
            yield return new WaitForSeconds(DelayTime);
            Tile tile = Pol.NextStep();

            if (tile)
            {
                UpdateKillAndSuffering(tile);
            }
            else
            {
                SceneManager.LoadScene(3);

            }


            TimeCounter++;
            if (TimeCounter >= TimeCritical)
            {
                DelayTime /= 2;
            }
            //ViewPol.drawPollution();
            viewUpdate();

            if (GetPollutionPercentage() > losePercentage)
            {
                SceneManager.LoadScene(2);
            }
        }


    }
    public float GetPollutionPercentage()
    {
        int pollutedCount = 0;
        for (int x = 0; x < FieldSize.x; x++)
            for (int y = 0; y < FieldSize.y; y++)
                if (Tiles[x, y].Pollution < 0)
                    pollutedCount++;

        return ((float)pollutedCount / (FieldSize.x * FieldSize.y));
    }

    public void viewUpdate()
    {
        vUI.UIUpdate();
    }

}
