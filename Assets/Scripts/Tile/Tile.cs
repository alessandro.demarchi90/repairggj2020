﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Vector2Int Position { get; private set; }
    public int Oxygen { get; private set; }
    public int Pollution { get; private set; }
    private Tile[,] Tiles;
    public Plant Plant { get; private set; }

    public List<Sprite> LandSprites;

    private SpriteRenderer spRenerer;
    private GameManager GM;

    public void Init(Tile[,] InTiles, Vector2Int InPosition)
    {
        Tiles = InTiles;
        Position = InPosition;
        spRenerer = GetComponent<SpriteRenderer>();
        GM = FindObjectOfType<GameManager>();
    }

    public bool CanBePolluted()
    {
        //if (Pollution <= -MyTags.MAX_CRITICAL_OXYGEN) return false;

        GameManager GM = GameManager.Get();

        Tile LeftTile = GM.GetTile(new Vector2Int(Position.x - 1, Position.y));
        if (LeftTile && LeftTile.Oxygen < MyTags.MAX_CRITICAL_OXYGEN && LeftTile.Pollution > -MyTags.MAX_CRITICAL_OXYGEN)
        {
            return true;
        }
        Tile RightTile = GM.GetTile(new Vector2Int(Position.x + 1, Position.y));
        if (RightTile && RightTile.Oxygen < MyTags.MAX_CRITICAL_OXYGEN && RightTile.Pollution > -MyTags.MAX_CRITICAL_OXYGEN)
        {
            return true;
        }
        Tile DownTile = GM.GetTile(new Vector2Int(Position.x, Position.y - 1));
        if (DownTile && DownTile.Oxygen < MyTags.MAX_CRITICAL_OXYGEN && DownTile.Pollution > -MyTags.MAX_CRITICAL_OXYGEN)
        {
            return true;
        }
        Tile UpTile = GM.GetTile(new Vector2Int(Position.x, Position.y + 1));
        if (UpTile && UpTile.Oxygen < MyTags.MAX_CRITICAL_OXYGEN && UpTile.Pollution > -MyTags.MAX_CRITICAL_OXYGEN)
        {
            return true;
        }
        return false;
    }

    public int GetHealth()
    {
        return Oxygen + Pollution;
    }

    public void Pollute()
    {
        if (Pollution > -MyTags.MAX_CRITICAL_OXYGEN)
        {
            Pollution--;
            UpdateTile();
            //GM.viewUpdate();
        }
    }

    public void Oxygenate()
    {
        ++Oxygen;
        UpdateTile();
        if (Plant && Plant.isSuffering)
        {
            Plant.StopSuffering();
        }

    }
    public void Deoxygenate()
    {
        --Oxygen;
        UpdateTile();
            //GM.viewUpdate();
    }

    public void PlantTree(Plant InPlant)
    {
        Plant = Instantiate(InPlant.gameObject, gameObject.transform).GetComponent<Plant>();
        Plant.InitPlant(this);
    }

    private void UpdateTile()
    {
        spRenerer.sprite = LandSprites[Mathf.Clamp(GetHealth(), -2, 3) + 2];
        GameManager GM = GameManager.Get();
        GM.ViewPol.DrawPollutionOnTile(this);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

}
