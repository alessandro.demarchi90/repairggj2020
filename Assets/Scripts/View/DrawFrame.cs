﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DrawFrame : MonoBehaviour
{
    //public Vector2Int fieldSize;
    public List<TileBase> Tiles;
    public Tilemap frame;

    private Vector2Int fieldSize;

    // Start is called before the first frame update
    void Start()
    {
        fieldSize = FindObjectOfType<GameManager>().FieldSize;
        // corners
        frame.SetTile(new Vector3Int(fieldSize.x, fieldSize.y, 1), Tiles[4]);
        frame.SetTile(new Vector3Int(-1, fieldSize.y, 1), Tiles[5]);
        frame.SetTile(new Vector3Int(fieldSize.x, -1, 1), Tiles[6]);
        frame.SetTile(new Vector3Int(-1, -1, 1), Tiles[7]);
        for (int x = 0; x < fieldSize.x; x++)
            {

            frame.SetTile(new Vector3Int(x, -1, 1), Tiles[1]); //bisogna invertire le coordinate X Y
            frame.SetTile(new Vector3Int(x, fieldSize.y, 1), Tiles[0]);

        }
        for (int y = 0; y < fieldSize.y; y++)
        {

            frame.SetTile(new Vector3Int(fieldSize.x, y, 1), Tiles[2]);
            frame.SetTile(new Vector3Int(-1, y, 1), Tiles[3]); //bisogna invertire le coordinate X Y

        }
    }

    // Update is called once per frame

}
