﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public GameObject button1;
    public GameObject button2;
    public GameObject seed;
    public GameObject grid;

    public GameObject pausePanel;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Pause();

    }
    public void Pause()
    {
        button1.SetActive(false);
        button2.SetActive(false);
        seed.SetActive(false);
        grid.SetActive(false);
        Time.timeScale = 0f;

        pausePanel.SetActive(true);
    }

    public void RetButton()
    {
        button1.SetActive(true);
        button2.SetActive(true);
        seed.SetActive(true);
        grid.SetActive(true);
        Time.timeScale = 1f;

        pausePanel.SetActive(false);
    }

    public void QuitButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
